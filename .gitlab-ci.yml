stages:
  - initial
  - test
  - deploy

# ================= INITIAL STAGE =================
# Test containers scripts are up to date
docker:
  stage: initial
  image: containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-python39
  script:
    - cd containers
    - python create_containers.py #HACK
    - git diff --exit-code

.test-python: &test-python
  stage: initial
  image: python
  script:
    - python -m pip install .
    - python -m pip list installed
    - python -c "import gwinferno"
    - for script in $(pip show -f gwinferno | grep "bin\/*.py" | xargs -I {} basename {}); do
          ${script} --help;
      done

basic-3.9:
  <<: *test-python
  image: python:3.9

basic-3.10:
  <<: *test-python
  image: python:3.10

.precommits: &precommits
  stage: initial
  script:
    - source activate $PYVERSION
    - mkdir -p $CACHE_DIR
    - pip install --upgrade pip
    - pip --cache-dir=$CACHE_DIR install .
    - pip --cache-dir=$CACHE_DIR install pre-commit
    # Run precommits (flake8, spellcheck, isort, no merge conflicts, etc)
    - pre-commit run --all-files --verbose --show-diff-on-failure

precommits-py3.9:
  <<: *precommits
  image: containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-python39
  variables:
    CACHE_DIR: ".pip39"
    PYVERSION: "python39"

precommits-py3.10:
  <<: *precommits
  image: containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-python310
  variables:
    CACHE_DIR: ".pip310"
    PYVERSION: "python310"

# ================= TEST STAGE =================
.unit-tests: &unit-test
  stage: test
  script:
    - python -m pip install .
    - python -m pip install pytest-cov
    - source tests/download_test_data.sh
    - pytest --cov gwinferno --color yes
    - python -m coverage report --show-missing

python-3.9:
  <<: *unit-test
  needs: ["basic-3.9", "precommits-py3.9"]
  image: containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-python39
  after_script:
    - coverage html
    - coverage xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - htmlcov/
    expire_in: 30 days

python-3.10:
  <<: *unit-test
  needs: ["basic-3.10", "precommits-py3.10"]
  image: containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-python310


# ================= DEPLOY STAGE =================
.build-container: &build-container
  stage: deploy
  image: docker:20.10.23
  needs: ["docker"]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        compare_to: 'refs/heads/main'
        paths:
          - containers/*
      when: manual
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - cd containers
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --tag v1-gwinferno-$PYVERSION - < v1-dockerfile-test-suite-$PYVERSION
    - docker image tag v1-gwinferno-$PYVERSION containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-$PYVERSION:latest
    - docker image push containers.ligo.org/bruce.edelman/gwinferno/v1-gwinferno-$PYVERSION:latest

build-python39-container:
  <<: *build-container
  variables:
    PYVERSION: "python39"

build-python310-container:
  <<: *build-container
  variables:
    PYVERSION: "python310"
