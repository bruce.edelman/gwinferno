=================================
Welcome to GWInferno's documentation!
=================================

.. automodule:: gwinferno
    :members:

----------------------------------
API:
----------------------------------

.. autosummary::
   :toctree: api
   :template: custom-module-template.rst
   :caption: API:
   :recursive:

    models
    parameter_estimation
    postprocess
    preprocess
