#!/bin/bash

conda create -n gwinferno python=3.10
conda activate gwinferno
conda install -c conda-forge numpyro  
conda install h5py  
pip install -r requirements.txt
python -m pip install -e .
conda deactivate